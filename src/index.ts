import * as express from 'express'
import * as http from 'http'
import * as morgan from 'morgan'
import { createConnection } from 'typeorm'
import * as swaggerUi from 'swagger-ui-express'
import * as compression from 'compression'
import { router } from './routes'
import { config } from './config'
import { swaggerSpec } from './micro/swagger'
import { corsConfig } from './micro/cors'
import { convertError, normalizeError, sendErrorToClient } from './micro/globalErrorHandlers'

let app = express()
app.server = http.createServer(app)
app.use(corsConfig)
app.use(compression())
app.use(morgan('dev'))
app.use(express.json())
app.use(express.urlencoded({extended: true}))
app.use(express.static('public'))
app.use('/api', router)
app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(swaggerSpec))

app.use([normalizeError, convertError, sendErrorToClient])
// load dependencies and start server
Promise.resolve().then(async () => {
  try {
    await createConnection()
  } catch (error) {
    console.log(error)
  }
  app.server.listen(config.port, () => {
    console.log(`Started on port ${config.port}`)
  })
})
