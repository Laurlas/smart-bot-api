import { Column, Entity, ManyToOne } from 'typeorm'
import { CustomEntity } from './CustomEntity'
import { User } from './User'

export enum DeviceType {
  ANDROID = 'android',
  IOS = 'ios'
}

@Entity({
  name: 'notification_tokens'
})
export class NotificationToken extends CustomEntity {
  static readonly ALL_RELATIONS = ['user']
  @ManyToOne(type => User, user => user.notificationTokens)
  user: User

  @Column({type: 'varchar', length: 50, select: true, nullable: false, unique: true})
  token: string

  @Column({
    type: 'enum',
    enum: DeviceType,
    default: DeviceType.ANDROID,
    nullable: false
  })
  device: DeviceType
}
