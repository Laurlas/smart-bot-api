import { BeforeInsert, BeforeUpdate, Column, Entity, OneToMany } from 'typeorm'
import { Token } from './Token'
import { CustomEntity } from './CustomEntity'
import * as bcrypt from 'bcryptjs'
import { Length } from 'class-validator'
import { IsEmailOrUndefined } from '../micro/decorators'
import { Bot } from './Bot'
import { NotificationToken } from './NotificationToken'

@Entity({
  name: 'users'
})
export class User extends CustomEntity {
  static readonly ALL_RELATIONS = ['codes']

  @Column({unique: true, length: 191, nullable: true, default: () => 'NULL'})
  @IsEmailOrUndefined()
  email: string

  @Column({precision: null, type: 'timestamp', nullable: true, default: () => 'NULL'})
  lastLogin: string

  @BeforeInsert()
  async hashPassword() {
    this.password = await bcrypt.hash(this.password, 10)
  }

  @BeforeUpdate()
  async hashUpdatePassword() {
    if (this.password) {
      this.password = await bcrypt.hash(this.password, 10)
    }
  }

  @Column({type: 'longtext', select: true, nullable: true})
  binanceApiKey: string

  @Column({type: 'longtext', select: true, nullable: true})
  binanceSecretKey: string

  @Column({type: 'boolean', default: false})
  validBinanceKey: boolean

  @Column({select: false})
  @Length(8, 40)
  password: string

  @OneToMany(type => Token, token => token.user)
  tokens: Token[]

  @OneToMany(type => Bot, bot => bot.user)
  bots: Bot[]

  @OneToMany(type => NotificationToken, notificationToken => notificationToken.user)
  notificationTokens: NotificationToken[]
}
