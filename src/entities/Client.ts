import {
  Entity,
  Column,
  OneToMany
} from 'typeorm'
import { Token } from './Token'
import { CustomEntity } from './CustomEntity'


@Entity({
  name: 'clients'
})
export class Client extends CustomEntity {
  @Column({unique: true, length: 191})
  name: string

  @Column({unique: true, length: 191})
  secret: string

  @Column({length: 191})
  description: string

  @Column({length: 191})
  domain: string

  @Column()
  enabled: boolean

  @OneToMany(type => Token, token => token.client)
  tokens: Token[]
}
