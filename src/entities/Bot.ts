import { Column, Entity, ManyToOne, OneToMany } from 'typeorm'
import { CustomEntity } from './CustomEntity'
import { User } from './User'
import { Order } from './Order'

@Entity({
  name: 'bots'
})
export class Bot extends CustomEntity {
  static readonly ALL_RELATIONS = ['user']

  @Column({type: 'longtext', select: true, nullable: false})
  market: string

  @Column({type: 'int', select: true, default: 0, nullable: false})
  investment: number

  @Column({type: 'float', unsigned: true, select: true, default: 2, nullable: false})
  ordersDistance: number

  @Column({type: 'float', unsigned: true, select: true, default: 2, nullable: false})
  sellProfit: number

  @ManyToOne(type => User, user => user.bots)
  user: User

  @OneToMany(type => Order, order => order.bot)
  orders: Order[]
}
