import {
  Entity,
  Column,
  ManyToOne
} from 'typeorm'
import { User } from './User'
import { Client } from './Client'
import { CustomEntity } from './CustomEntity'

@Entity({
  name: 'tokens'
})
export class Token extends CustomEntity {
  @Column({type: 'text'})
  token: string

  @Column({length: 191})
  refresh: string

  @Column({length: 191})
  userAgent: string

  @Column({length: 191})
  ipAddress: string

  @ManyToOne(type => User, user => user.tokens)
  user: User

  @ManyToOne(type => Client, client => client.tokens)
  client: Client

}
