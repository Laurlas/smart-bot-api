import {
    BaseEntity,
    CreateDateColumn, DeleteDateColumn, PrimaryGeneratedColumn,
    UpdateDateColumn
} from 'typeorm'
import APIError from '../helpers/APIError'
import * as httpStatus from 'http-status'

export class CustomEntity extends BaseEntity {
    @PrimaryGeneratedColumn('uuid')
    id: string
    @CreateDateColumn({precision: null, type: 'timestamp', default: () => 'CURRENT_TIMESTAMP'})
    createdAt: Date
    @UpdateDateColumn({
        precision: null,
        type: 'timestamp',
        nullable: true,
        default: () => 'NULL',
        onUpdate: 'CURRENT_TIMESTAMP'
    })
    updatedAt: Date

    @DeleteDateColumn({ name: 'deletedAt' })
    deletedAt?: Date

    static async findOrFail<T extends BaseEntity>(id?: any, options?): Promise<T | APIError | BaseEntity> {
        if (!options) {
            options = {select: ['id']}
        }
        let found = await this.findOne(id, options)
        if (!found) {
            throw new APIError(`${this.name} not found`, httpStatus.NOT_FOUND)
        }
        return found
    }

    static async failIfFound<T extends BaseEntity>(options): Promise<T | APIError | BaseEntity> {
        let found = await this.findOne(options)
        if (found) {
            throw new APIError(`${this.name} already exists`, httpStatus.CONFLICT)
        }
        return found
    }
}
