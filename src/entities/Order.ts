import { Column, Entity, OneToOne, JoinColumn, ManyToOne } from 'typeorm'
import { CustomEntity } from './CustomEntity'
import { Bot } from './Bot'

export enum OrderType {
  BUY = 'buy',
  SELL = 'sell'
}

@Entity({
  name: 'orders'
})
export class Order extends CustomEntity {
  static readonly ALL_RELATIONS = ['sellOrder']

  @Column({
    type: 'enum',
    enum: OrderType,
    default: OrderType.BUY,
    nullable: false
  })
  type: OrderType

  @Column({type: 'float', select: true, default: 0, nullable: false})
  exchangeRate: number

  @Column({type: 'float', select: true, default: 0, nullable: false})
  amount: number

  @Column({type: 'float', select: true, default: 0, nullable: false})
  value: number

  @Column({type: 'float', select: true, default: 0, nullable: false})
  commission: number

  @Column({
    precision: null,
    type: 'timestamp',
    nullable: true,
    default: () => 'NULL',
  })
  filledAt: Date

  @ManyToOne(type => Bot, bot => bot.orders)
  bot: Bot

  @OneToOne(type => Order, sellOrder => sellOrder.id)
  @JoinColumn()
  sellOrder: Order

  @Column({type: 'varchar', length: 10, select: true, nullable: false})
  externalId: string
}
