import fs from 'fs'
import * as passport from 'passport'
import * as jwt from 'jsonwebtoken'
import { BasicStrategy } from 'passport-http'
import { Strategy as BearerStrategy } from 'passport-http-bearer'
import { Strategy as ClientPasswordStrategy } from 'passport-oauth2-client-password/lib/index'
import * as headerHelper from '../helpers/headerParser'
import { GrantTypeError } from '../exceptions/grantTypeError'
import { NotFoundError } from '../exceptions/notFoundError'
import { ExpiredTokenError } from '../exceptions/expiredTokenError'
import { config } from '../config'
import { Client } from '../entities/Client'
import { fillClientCredentials } from './proxy'
import { Token } from '../entities/Token'
import * as moment from 'moment'
import APIError from '../helpers/APIError'
import * as httpStatus from 'http-status'
import * as AnonymousStrategy from 'passport-anonymous'

passport.serializeUser(function (user, done) {
  done(null, user)
})

passport.deserializeUser(function (user, done) {
  done(null, user)
})
passport.use(new AnonymousStrategy())

//  Client authentication with credentials into header
passport.use('client_credentials', new BasicStrategy(
  async function (clientId, clientSecret, callback) {
    if (config.oauth2.grants.indexOf('client_credentials') < 0) {
      return callback(new GrantTypeError('Grant type err'))
    }
    let result = await validateClientCredentials(clientId, clientSecret)
    if (result instanceof Error) {
      return callback(null, false)
    }
    return callback(null, result)
  }
))

//  Client authentication with credentials into body
passport.use('password', new ClientPasswordStrategy(
  async function (clientId, clientSecret, callback) {
    if (config.oauth2.grants.indexOf('password') < 0) {
      return callback(new GrantTypeError('Grant type error'))
    }
    let client = await validateClientCredentials(clientId, clientSecret)
    if (client instanceof Error) {
      return callback(new NotFoundError('Client not found', httpStatus.UNAUTHORIZED))
    }
    return callback(null, client, {scope: '*'})
  }
))

//  Bearer authentication
passport.use('bearer', new BearerStrategy({passReqToCallback: true},
  async function (req, accessToken, callback) {
    if (config.oauth2.authentications.indexOf('bearer') < 0) {
      return callback(new GrantTypeError('Grant type error'))
    }
    let ipAddress = headerHelper.getIP(req)
    let userAgent = headerHelper.getUA(req)
    let result = await validateBearerToken(accessToken, ipAddress, userAgent)
    if (result instanceof ExpiredTokenError) {
      return callback(new APIError('token_expired', httpStatus.extra.iis.LOGIN_TIME_OUT))
    } else if (result instanceof Error) {
      return callback(new APIError('unauthorized', httpStatus.UNAUTHORIZED))
    }
    return callback(null, result, {scope: '*'})
  }
))

//  Public method for client validation
const validateClientCredentials = async function (name, secret) {
  const client = await Client.findOne({name: name, secret: secret})
  if (!client) {
    return new NotFoundError('Client not found')
  }
  if (client.secret !== secret) {
    return new NotFoundError('Client not found')
  }
  return client
}

// Public method for token validation
const validateBearerToken = async function (token, ipAddress, userAgent) {
  let entityToken = await Token.findOne({token: token}, {relations: ['user', 'client']})
  if (!entityToken) {
    return new NotFoundError('Token not found', httpStatus.UNAUTHORIZED)
  }
  let diff = moment().diff(entityToken.createdAt, 'second')
  if (diff > config.token.life) {
    return new ExpiredTokenError('Token expired')
  }
  // Check jwt token
  let verified: any = false
  if (config.token.jwt.enabled === true) {
    verified = await verifyJwtToken(entityToken, ipAddress, userAgent)
    if (verified instanceof Error) {
      return verified
    }
  }
  if (verified) {
    if (entityToken.user) {
      return entityToken.user
    }
    if (entityToken.client) {
      return entityToken.client
    }
  }
  return false
}

//  Verify if JWT is valid
const verifyJwtToken = async function (token, ipAddress, userAgent) {
  let secret = config.token.jwt.secretKey
  if (config.token.jwt.cert !== null) {
    secret = fs.readFileSync(config.token.jwt.cert).toString()
  }

  return await jwt.verify(token.token, secret, {ignoreExpiration: false}, function (err, decoded) {
    if (!err) {
      if ((config.token.jwt.uacheck === false || decoded.bua === null || decoded.bua === userAgent) && (config.token.jwt.ipcheck === false || decoded.ipa === ipAddress)) {
        return true
      }
      return new NotFoundError('Token not valid')
    } else if (err.name && err.name === 'TokenExpiredError') {
      return new ExpiredTokenError('Token expired')
    } else {
      console.log('err JWT')
      throw err
    }
  })
}

export const isClientAuthenticated = [
  fillClientCredentials,
  passport.authenticate(['client_credentials', 'password'], {session: false})
]
export const isAuthenticated = passport.authenticate(['bearer'], {session: false})
export const isOptionalAuthenticated = passport.authenticate(['bearer', 'anonymous'], {session: false})
