import { Client } from '../entities/Client'
import { config } from '../config'
import * as headerHelper from '../helpers/headerParser'
import * as validator from '../helpers/paramChecker'
import { NotFoundError } from '../exceptions/notFoundError'
import bcrypt from 'bcryptjs'

const consts = {
  validation: {
    types: {
      boolean: 'bool',
      integer: 'int',
      email: 'email',
      domain: 'domain',
      name: 'name',
      username: 'username',
      text: 'text',
      date: 'date',
      password: 'password',
      mixed: 'mixed'
    }
  }
}

export const fillClientCredentials = async function (req, res, callback) {
  // Input validation
  if (config.validation.enabled === true) {
    try {
      req.body['username'] = validator.sanitizeParam(consts.validation.types.mixed, req.body['username'], true, config.validation.username)
      req.body['password'] = validator.sanitizeParam(consts.validation.types.mixed, req.body['password'], true, config.validation.password)
      req.body['client_id'] = validator.sanitizeParam(consts.validation.types.mixed, req.body['client_id'], true, config.validation.clientId)
      req.body['client_secret'] = validator.sanitizeParam(consts.validation.types.mixed, req.body['client_secret'], true, config.validation.clientSecret)
    } catch (err) {
      console.log('validation error')
      return callback(err)
    }
  }

  let isBasicAuth = false
  let grant = req.body['grant_type']
  let auth = req.headers['authorization']
  let username = req.body['username']
  let password = req.body['password']
  let clientId = req.body['client_id']
  let clientSecret = req.body['client_secret']

  // Add ip address and user agent to request
  req.body['userAgent_auto_generated'] = headerHelper.getUA(req)
  req.body['ipAddress_auto_generated'] = headerHelper.getIP(req)

  if (auth && auth.indexOf('Basic') >= 0) {
    isBasicAuth = true
  }

  if (!config.oauth2.useClientSecret && grant !== 'client_credentials' && clientId && !clientSecret && !isBasicAuth) {
    let client = await Client.findOne({name: clientId, enabled: true})
    if (!client) {
      return new NotFoundError('Client not found')
    }
    req.body['client_secret'] = bcrypt.compare(client.secret, config.cryptonSecret)
    return callback(null, true, {scope: '*'})
  } else {
    return callback()
  }
}
