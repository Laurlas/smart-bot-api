export const getUA = function (req) {
  let ua = null
  if (req) {
    ua = req.headers['user-agent']
  }
  return ua
}

export const getBearerToken = function (req) {
  let token = null
  let auth = req.headers['authorization']
  if (req && auth) {
    token = auth.replace(/bearer/ig, '').trim()
  }
  return token
}

export const getBasicAuthentication = function (req) {
  let credentials = null
  let auth = req.headers['authorization']
  if (req && auth && (auth.indexOf('Basic') >= 0 || auth.indexOf('basic') >= 0 || auth.indexOf('BASIC') >= 0)) {
    let b64Credentials = auth.replace(/basic/ig, '').trim()
    let buf = new Buffer(b64Credentials, 'base64')
    let temp = buf.toString().split(':')
    credentials = {
      'username': temp[0],
      'password': temp[1]
    }
  }
  return credentials
}

export const getIP = function (req) {
  let ipAddress = null
  if (req) {
    ipAddress = req.headers['x-forwarded-for'] ||
      req.connection.remoteAddress ||
      req.socket.remoteAddress
  }
  return ipAddress
}
