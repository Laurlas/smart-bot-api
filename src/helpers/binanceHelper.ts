import * as marketMinimums from '../../minimums.json'
import { promises as fs } from 'fs'

export const getMarketDetails = async (binanceApi, market) => {
  if (marketMinimums[market]) {
    return marketMinimums[market]
  }
  const data = await binanceApi.exchangeInfo()
  let minimums = {}
  for (let obj of data.symbols) {
    let filters = {status: obj.status} as any
    for (let filter of obj.filters) {
      if (filter.filterType == 'MIN_NOTIONAL') {
        filters.minNotional = filter.minNotional
      } else if (filter.filterType == 'PRICE_FILTER') {
        filters.minPrice = filter.minPrice
        filters.maxPrice = filter.maxPrice
        filters.tickSize = filter.tickSize
      } else if (filter.filterType == 'LOT_SIZE') {
        filters.stepSize = filter.stepSize
        filters.minQty = filter.minQty
        filters.maxQty = filter.maxQty
      }
    }
    //filters.baseAssetPrecision = obj.baseAssetPrecision;
    //filters.quoteAssetPrecision = obj.quoteAssetPrecision;
    filters.orderTypes = obj.orderTypes
    filters.icebergAllowed = obj.icebergAllowed
    minimums[obj.symbol] = filters
  }
  await fs.writeFile('minimums.json', JSON.stringify(minimums, null, 4))
  return minimums[market]
}