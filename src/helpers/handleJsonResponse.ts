export const Error = function (res, err) {
  let template = {
    status: 'ERR',
    type: err.name,
    message: err.message,
    stack: err.stack
  }

  let json = JSON.stringify(template)
  res.setHeader('Content-Type', 'application/json')
  res.setHeader('Cache-Control', 'no-store')
  res.setHeader('Pragma', 'no-cache')
  res.statusCode = err.status || 500

  res.end(json)
}

export const Success = function (res, extra) {
  let ret = {
    status: 'OK',
    data: extra
  }

  let json = JSON.stringify(ret)
  res.setHeader('Content-Type', 'application/json')
  res.setHeader('Cache-Control', 'no-store')
  res.setHeader('Pragma', 'no-cache')
  res.statusCode = 200

  res.end(json)
}

