import { Expo } from 'expo-server-sdk'
import { NotificationToken } from '../entities/NotificationToken'

class ExpoService {
  #expoProvider

  constructor() {
    this.#expoProvider = new Expo()
  }

  async sendPushNotificationToInternalUser(to, title, body) {
    const tokenObjects = await NotificationToken.find({user: to})
    const tokens = tokenObjects.map((tokenObject) => tokenObject.token)
    await this.sendPushNotification(tokens, title, body)
  }

  async sendPushNotification(to = [], title, body) {
    let messages = []
    for (let i = 0; i < to.length; i++) {
      if (!Expo.isExpoPushToken(to[i])) {
        return
      }
      messages.push({
        to: to[i],
        sound: 'default',
        title,
        body
      })
    }
    let chunks = this.#expoProvider.chunkPushNotifications(messages)
    for (let chunk of chunks) {
      try {
        await this.#expoProvider.sendPushNotificationsAsync(chunk)
      } catch (error) {
        console.error(error)
      }
    }
  }
}

export default new ExpoService()