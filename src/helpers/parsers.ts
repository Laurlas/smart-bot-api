export const countDecimals = (value: string) => {
  const floatValue = parseFloat(value)
  if ((floatValue % 1) != 0) {
    return floatValue.toString().split('.')[1].length
  }
  return 0
}
export const toFixedFloor = (num, fixed) => {
  var re = new RegExp('^-?\\d+(?:\.\\d{0,' + (fixed || -1) + '})?')
  return num.toString().match(re)[0]
}