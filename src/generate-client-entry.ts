import * as uuid from 'uuid'
import {Client} from './entities/Client'
import {createConnection, getRepository} from "typeorm";

const allowed = ['mobile']
const usage = `\n\nUsage: npm run genereate-client -- <type>   Generate new client of type where type in: ${allowed.join(', ')}`
if (process.argv.length < 3) {
  console.log(`Missing Argument: type${usage}`)
  process.exit(1)
}
if (process.argv.length > 3) {
  console.log(`Too many arguments${usage}`)
  process.exit(1)
}

const type = process.argv[2]
if (!allowed.includes(type)) {
  console.log(`Specified type is invalid${usage}`)
  process.exit(1)
}

Promise.resolve().then(async () => {
  try {
    await createConnection()
    const secret = Math.round((Math.pow(36, 16 + 1) - Math.random() * Math.pow(36, 16))).toString(36).slice(1)
    const id = uuid.v1()
    const clientName = `${type}_${id}`
    const client = new Client()
    client.id = id
    client.name = clientName
    client.secret = secret
    client.domain = 'http://localhost'
    client.description = `${type[0].toUpperCase()}${type.slice(1)} client`
    client.enabled = true

    await getRepository(Client).save(client)

    console.log('\nClient generated successfully\n')
    console.log('`client_id`: `' + clientName + '`')
    console.log('`client_secret`: `' + secret + '`\n')
    process.exit(0)
  } catch (error) {
    console.log(error)
    process.exit(1)
  }
})
