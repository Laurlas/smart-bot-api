import * as util from 'util'

export const NotFoundError = function (message, status = 500) {
  Error.captureStackTrace(this, this.constructor)
  this.name = this.constructor.name
  this.message = message
  this.status = status
}
util.inherits(NotFoundError, Error)
