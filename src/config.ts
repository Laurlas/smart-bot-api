import * as dotenvSafe from 'dotenv-safe'

let dotEnvOptions = undefined
if (process.env.NODE_ENV === 'production') {
  dotEnvOptions = {
    example: '.env.production.example',
    path: '.env.production'
  }
}
dotenvSafe.config(dotEnvOptions)
export const config = Object.freeze({
  env: process.env.NODE_ENV,
  port: process.env.SERVER_PORT,
  corsAddress: process.env.CORS_ADDR,
  cryptonSecret: process.env.CRYPTON_SECRET,
  db: {
    host: process.env.TYPEORM_HOST,
    port: Number(process.env.TYPEORM_PORT),
    user: process.env.TYPEORM_USERNAME,
    password: process.env.TYPEORM_PASSWORD,
    database: process.env.TYPEORM_DATABASE
  },
  oauth2: {
    useClientSecret: true,
    authentications: ['bearer'],
    grants: ['client_credentials', 'password', 'refresh_token', 'authorization_code']
  },
  token: {
    life: 3600 * 24 * 10,
    length: 64, // bytes
    autoRemove: false,
    jwt: {
      enabled: true,
      ipcheck: false,
      uacheck: false,
      secretKey: process.env.JWT_SECRET,
      cert: null,
      algorithm: 'AES-256-CBC'
    }
  },
  validation: {
    // Enables input validation
    enabled: false,
    // Regexp for username
    username: /^[\w\.]{2,100}$/g,
    // Regexp for password
    password: /^(?=.*[A-Za-z])(?=.*\d)(?=.*[.)(=,|$@$!%*#?&])[A-Za-z\d.)(=, | $@ $!%*#?&]{8,255}$/g,
    // Regexp for client name
    clientId: /^[\w\.]{2,100}$/g,
    // Regexp for client secret
    clientSecret: /^(?=.*[A-Za-z])(?=.*\d)(?=.*[.)(=,|$@$!%*#?&])[A-Za-z\d.)(=, | $@ $!%*#?&]{8,255}$/g
  }
})
