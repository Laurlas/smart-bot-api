import { User } from '../entities/User'
import APIError from '../helpers/APIError'
import { validate } from 'class-validator'
import * as httpStatus from 'http-status'
import * as Binance from 'node-binance-api'
import { NotificationToken } from '../entities/NotificationToken'

export default class UserController {
  static async getCurrentUser({user}, res) {
    return res.json(user)
  }

  static async saveUser(req, res) {
    const {user: userBody} = req.body
    await User.failIfFound({email: userBody.email})
    if (!userBody.password || userBody.password.length < 8) {
      throw new APIError(`password must be at least 8 characters long!`, httpStatus.EXPECTATION_FAILED)
    }
    let user = User.create(userBody)
    const errors = await validate(user)
    if (errors.length > 0) {
      throw new APIError(`validation_failed!`, httpStatus.EXPECTATION_FAILED, errors)
    }
    user = await User.save(user)
    return res.json(user)
  }

  static async registerNotificationToken(req, res) {
    const {token, device} = req.params
    let notificationToken = NotificationToken.create({token, device, user: {id: req.user.id}})
    try {
      await NotificationToken.save(notificationToken)
    } catch (e) {
      return res.json(true)
    }
    return res.json(true)
  }

  static async updateUser(req, res) {
    const {user: existingUser, body: {user: userBody}} = req as { user: User, body: { user: User } }
    // let sameUserWithPassword = <User>(await User.findOne({id: existingUser.id}, {select: ['id', 'password']}))
    let user = User.create({...userBody, id: existingUser.id})
    if (userBody.binanceApiKey !== undefined || userBody.binanceSecretKey !== undefined) {
      const binanceApi = new Binance().options({
        APIKEY: userBody.binanceApiKey,
        APISECRET: userBody.binanceSecretKey
      })
      const accountData = await binanceApi.futuresAccount()
      if (!accountData?.canTrade) {
        user = User.create({id: existingUser.id, validBinanceKey: false})
        await User.save(user)
        throw new APIError(`Invalid binance api key or secret!`, httpStatus.BAD_REQUEST)
      }
    }
    user.validBinanceKey = true
    await User.save(user)
    const updatedUser = <User>(await User.findOne(existingUser.id))
    return res.json(updatedUser)
  }

}
