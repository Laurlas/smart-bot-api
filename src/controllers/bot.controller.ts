import APIError from '../helpers/APIError'
import { validate } from 'class-validator'
import * as httpStatus from 'http-status'
import { Bot } from '../entities/Bot'
import * as Binance from 'node-binance-api'
import { countDecimals, toFixedFloor } from '../helpers/parsers'
import { Order, OrderType } from '../entities/Order'
import { getMarketDetails } from '../helpers/binanceHelper'

export default class BotController {
  static async getBots(req, res) {
    const bot = await Bot.find({user: req.user.id})
    return res.json(bot)
  }

  static async getBot(req, res) {
    const {dateInterval = '%d-%m-%Y'} = req.query
    const bot = await Bot.findOrFail(req.params.id, {}) as Bot
    const binanceApi = new Binance().options({
      APIKEY: req.user.binanceApiKey,
      APISECRET: req.user.binanceSecretKey
    })
    const [orders, price] = await Promise.all([
      binanceApi.openOrders(bot.market),
      binanceApi.prices(bot.market)
    ])
    const profitStats = await Bot.query(`
      select profit, fillDate, profit * 100 / investment as percentageProfit
      from (
               select b.investment,
                      SUM(
                              IF(
                                      se.filledAt is not null,
                                      se.value - bo.value - bo.commission * bo.exchangeRate -
                                      se.commission * se.exchangeRate,
                                      ? * bo.amount - bo.value - bo.commission * bo.exchangeRate
                                  )
                          ) AS profit,
                      DATE_FORMAT(
                              IF(
                                      se.filledAt is not null,
                                      se.filledAt,
                                      bo.filledAt
                                  ),
                                  '${dateInterval}'
                          ) as fillDate
               from bots b
                        inner join orders bo on b.id = bo.botId
                        left join orders se on bo.sellOrderId = se.id
               where bo.type = ?
                 and se.type = ?
                 and bo.filledAt is not null
                 and b.deletedAt is null
                 and bo.botId = ?
               group by fillDate
           ) q1
      order by fillDate
  `, [price[bot.market], OrderType.BUY, OrderType.SELL, bot.id])
    const [totalProfitStats] = await Bot.query(`
      select profit, profit * 100 / investment as percentageProfit
      from (
               select b.investment,
                      SUM(
                              IF(
                                      se.filledAt is not null,
                                      se.value - bo.value - bo.commission * bo.exchangeRate -
                                      se.commission * se.exchangeRate,
                                      ? * bo.amount - bo.value - bo.commission * bo.exchangeRate
                                  )
                          ) AS profit
               from bots b
                        inner join orders bo on b.id = bo.botId
                        left join orders se on bo.sellOrderId = se.id
               where bo.type = ?
                 and se.type = ?
                 and bo.filledAt is not null
                 and b.deletedAt is null
                 and bo.botId = ?
           ) q1
  `, [price[bot.market], OrderType.BUY, OrderType.SELL, bot.id])
    return res.json({...bot, orders, price: price[bot.market], profitStats, totalProfitStats})
  }

  static async saveBot(req, res) {
    const {bot: botBody} = req.body as { bot: Bot }
    let {market, ordersDistance, sellProfit, investment} = botBody
    investment = parseInt(String(investment), 10)
    if (!market) {
      throw new APIError(`Invalid Market`, httpStatus.EXPECTATION_FAILED)
    }
    if (!ordersDistance || ordersDistance <= 0 || ordersDistance > 50) {
      throw new APIError(`The distance between orders must be greater than 0 and lower than 50`, httpStatus.EXPECTATION_FAILED)
    }
    if (!sellProfit || sellProfit <= 0 || sellProfit > 50) {
      throw new APIError(`The sell profit must be greater than 0 and lower than 50`, httpStatus.EXPECTATION_FAILED)
    }
    if (!investment || investment < 0) {
      throw new APIError(`Please increase investment`, httpStatus.EXPECTATION_FAILED)
    }
    await Bot.failIfFound({user: req.user.id, market: botBody.market})
    const binanceApi = new Binance().options({
      APIKEY: req.user.binanceApiKey,
      APISECRET: req.user.binanceSecretKey
    })
    const balances = await binanceApi.balance()
    const usdtBalance = parseFloat(balances['USDT'] && balances['USDT'].available)
    if (usdtBalance < investment) {
      throw new APIError(`You have only ${usdtBalance} USDT available. Decrease investment or deposit more USDT on binance to continue`, httpStatus.EXPECTATION_FAILED)
    }

    botBody.user = req.user
    let bot = Bot.create(botBody)
    const errors = await validate(bot)
    if (errors.length > 0) {
      throw new APIError(`validation_failed!`, httpStatus.EXPECTATION_FAILED, errors)
    }

    const marketDetails = await getMarketDetails(binanceApi, market)
    const usdtSpendNow = investment / 2
    const marketPrice = (await binanceApi.prices(market))[market]
    const marketBuyAmount = toFixedFloor((usdtSpendNow / marketPrice), countDecimals(marketDetails.stepSize))
    if (marketBuyAmount * marketPrice < parseFloat(marketDetails.minNotional) * 2 * 1.1) {
      throw new APIError(`Your investment needs to be greater than ${parseInt(marketDetails.minNotional, 10) * 4 * 1.1} to continue`, httpStatus.EXPECTATION_FAILED)
    }
    bot = await Bot.save(bot)
    const buyGrids = ~~(usdtSpendNow / (parseFloat(marketDetails.minNotional) * 1.1))
    const buyQuantityPerGrid = toFixedFloor(marketBuyAmount / buyGrids, countDecimals(marketDetails.stepSize))

    for (let i = 1; i <= buyGrids; i++) {
      try {
        console.info(`Buying ${buyQuantityPerGrid} ${market} worth ${buyQuantityPerGrid * marketPrice} USDT`)
        const buyResult = await binanceApi.marketBuy(market, buyQuantityPerGrid)
        const {executedQty, fills, orderId} = buyResult
        const commission = fills.reduce((a, b) => a + parseFloat(b?.commission || 0), 0)
        const avgPrice = (fills.reduce((a, b) => a + parseFloat(b?.price || marketPrice), 0)) / fills.length
        const value = (fills.reduce((a, b) => a + parseFloat(b?.price || marketPrice) * parseFloat(b.qty), 0)) / fills.length
        let internalBuyOrder = Order.create({
          type: OrderType.BUY,
          exchangeRate: avgPrice,
          amount: parseFloat(executedQty),
          value,
          commission,
          filledAt: new Date(),
          externalId: orderId,
          bot: {id: bot.id}
        })
        internalBuyOrder = await Order.save(internalBuyOrder)

        const marketAmount = parseFloat(executedQty) - commission
        const sellQuantity = toFixedFloor(marketAmount, countDecimals(marketDetails.stepSize))
        const toSellPrice = toFixedFloor((100 + i * bot.sellProfit) * avgPrice / 100, countDecimals(marketDetails.tickSize))
        console.info(`Placing sell order ${sellQuantity} ${market} at ${toSellPrice} USDT`)
        const sellResult = await binanceApi.sell(market, sellQuantity, toSellPrice)
        let internalSellOrder = Order.create({
          type: OrderType.SELL,
          exchangeRate: parseFloat(sellResult.price),
          amount: sellQuantity,
          value: sellQuantity * parseFloat(sellResult.price),
          commission,
          filledAt: null,
          externalId: sellResult.orderId,
          bot: {id: bot.id}
        })
        internalSellOrder = await Order.save(internalSellOrder)
        await Order.update({id: internalBuyOrder.id}, {sellOrder: {id: internalSellOrder.id}})

        const limitBuyPrice = toFixedFloor((100 - i * bot.ordersDistance) * marketPrice / 100, countDecimals(marketDetails.tickSize))
        const limitBuyResult = await binanceApi.buy(market, buyQuantityPerGrid, limitBuyPrice)
        let internalBuyLimitOrder = Order.create({
          type: OrderType.BUY,
          exchangeRate: parseFloat(limitBuyResult.price),
          amount: buyQuantityPerGrid,
          value: buyQuantityPerGrid * parseFloat(limitBuyResult.price),
          commission,
          filledAt: null,
          externalId: limitBuyResult.orderId,
          bot: {id: bot.id}
        })
        internalBuyLimitOrder = await Order.save(internalBuyLimitOrder)
      } catch (e) {
        console.error(e.body)
      }
    }
    const {
      user,
      ...botData
    } = bot
    return res.json(botData)
  }


  static async closeBot(req, res) {
    const {sell} = req.body as { sell: boolean }
    const bot = await Bot.findOrFail(req.params.id, {}) as Bot
    const binanceApi = new Binance().options({
      APIKEY: req.user.binanceApiKey,
      APISECRET: req.user.binanceSecretKey
    })
    try {
      await binanceApi.cancelAll(bot.market)
    } catch (e) {
      console.error(`Couldn't cancel orders`)
      console.error(e.body)
    }
    if (sell) {
      const balances = await binanceApi.balance()
      const coin = bot.market.replace('USDT', '')
      if (balances[coin] && balances[coin].available) {
        const marketDetails = await getMarketDetails(binanceApi, bot.market)
        const toSell = toFixedFloor(balances[coin].available, countDecimals(marketDetails.stepSize))
        if (parseFloat(toSell) > parseFloat(marketDetails.minNotional)) {
          try {
            await binanceApi.marketSell(bot.market, toSell)
          } catch (e) {
            console.error(`Tried to sell ${toSell} ${bot.market}`)
            console.error(e.body)
          }
        }
      }
    }
    await Bot.softRemove(bot)
    return res.json(true)
  }
}
