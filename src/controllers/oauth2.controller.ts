import * as oauth2orize from 'oauth2orize'
import fs from 'fs'
import * as jwt from 'jsonwebtoken'
import * as headerHelper from '../helpers/headerParser'
import { NotFoundError } from '../exceptions/notFoundError'
import * as handleJsonResponse from '../helpers/handleJsonResponse'
import { Client } from '../entities/Client'
import { Token } from '../entities/Token'
import { User } from '../entities/User'
import { config } from '../config'
import * as bcrypt from 'bcryptjs'
import * as crypto from 'crypto'
import APIError from '../helpers/APIError'
import * as httpStatus from 'http-status'
import * as moment from 'moment'

const handleSuccess = handleJsonResponse.Success

// Create OAuth 2.0 server
let server = oauth2orize.createServer()

// Register serialialization function
server.serializeClient(function (client, callback) {
  return callback(null, client.id)
})

// Register deserialization function
server.deserializeClient(async function (id, callback) {
  return callback(await Client.findOne({enabled: true, id: id}))
})

// Exchange credentials for access token (resource owner password credentials grant)
server.exchange(oauth2orize.exchange.password(async function (client, username, password, scope, req, callback) {
  const ret: any = await exchangePassword(client, username, password, req['ipAddress_auto_generated'], req['userAgent_auto_generated'])
  if (ret instanceof Error) {
    return callback(ret)
  }
  return callback(null, ret.access_token, ret.refresh_token, {'expires_in': ret.expires_in})
}))

// Exchange refresh token with new access token (refresh token grant)
server.exchange(oauth2orize.exchange.refreshToken(async function (client, refreshToken, callback) {
  const ret: any = await exchangeRefreshToken(client, refreshToken)
  if (ret instanceof Error) {
    return callback(ret)
  }
  return callback(null, ret.access_token, ret.refresh_token, {expires_in: ret.expires_in})
}))

// Exchange credentials for access token (client credentials grant)
server.exchange(oauth2orize.exchange.clientCredentials(async function (client, scope, req, callback) {
  const ret: any = await exchangeClientCredentials(client, req['ipAddress_auto_generated'], req['userAgent_auto_generated'])
  if (ret instanceof Error) {
    return callback(ret)
  }
  return callback(null, ret.access_token, ret.refresh_token, {expires_in: ret.expires_in})
}))

// Delete all tokens associated to the user
export const doLogout = async function (req, res) {
  const token = await headerHelper.getBearerToken(req)
  let all = await req.body['force']
  return handleSuccess(res, await revokeToken(token, all))
}

// Create access token (jwt if enabled) and refresh token by client and user id
const createTokens = async function (client, userId, req) {
  try {
    let jwtToken = await createJwtToken(client, userId, req['ipAddress_auto_generated'] || null, req['userAgent_auto_generated'] || null)
    let refreshToken = config.oauth2.grants.indexOf('refresh_token') >= 0 ? await uid(config.token.length) : null
    await Token.create({
      token: jwtToken.toString(),
      refresh: refreshToken,
      client: client.id,
      user: userId,
      ipAddress: req['ipAddress_auto_generated'] || 'local',
      userAgent: req['userAgent_auto_generated'] || 'user agent'
    }).save()

    let user = await User.create({lastLogin: moment().format('YYYY-MM-DD HH:mm'), id: userId})
    await User.save(user)

    let obj = {}
    obj['token_type'] = 'Bearer'
    obj['access_token'] = jwtToken
    obj['expires_in'] = config.token.life
    obj['refresh_token'] = refreshToken
    return obj
  } catch (err) {
    console.log('Token generate -- ERROR')
    return err
  }
}

// Create a JWT token
const createJwtToken = async function (client, userId, ipAddress, userAgent) {
  if (config.token.jwt.enabled === false) {
    return uid(config.token.length)
  }
  const jtiVal = uid(16)
  const createdAt = moment().unix()
  let token = null
  let claim = {
    'iss': client.name,
    'sub': userId ? userId : client.id,
    'aud': client.domain,
    'ipa': ipAddress,
    'bua': userAgent,
    'jti': jtiVal,
    'iat': createdAt
  }

  if (config.token.jwt.cert !== null) {
    let cert = fs.readFileSync(config.token.jwt.cert)
    token = jwt.sign(
      claim,
      cert,
      {algorithm: config.token.jwt.algorithm, expiresIn: config.token.life}
    )
  } else {
    token = jwt.sign(
      claim,
      config.token.jwt.secretKey,
      {expiresIn: config.token.life}
    )
  }
  return token
}

// Create a random token
const uid = async function (len) {
  let hash = await crypto.randomBytes(len)
  return hash.toString('hex')
}

// Public method for password exchange
const exchangePassword = async function (client, username, password, ip, userAgent) {
  let req = {}
  req['ipAddress_auto_generated'] = ip
  req['userAgent_auto_generated'] = userAgent
  // search user by email
  let user = await User.findOne({email: username}, {select:['id', 'email', 'password']})
  if (!user) {
    return new NotFoundError('User not found')
  }
  password = password.toString()
  // compare password
  let isMatch = await bcrypt.compare(password, user.password)
  if (!isMatch) {
    return new APIError('password_mismatch', httpStatus.UNAUTHORIZED)
  }
  //let tokens = await Token.find({user: user})
  return createTokens(client, user.id, req)
}
// Public method for client credentials exchange
const exchangeClientCredentials = async function (client, ip, userAgent) {
  let req = {}
  req['ipAddress_auto_generated'] = ip
  req['userAgent_auto_generated'] = userAgent
  if (config.token.autoRemove === true) {
    await Token.delete({client: client.id})
  }
  return createTokens(client, null, req)
}

// Public method for refresh token exchange
const exchangeRefreshToken = async function (client, refreshToken) {
  const token = await Token.findOne({refresh: refreshToken}, {relations: ['user']})
  if (!token) {
    return new APIError('token_not_found', httpStatus.UNAUTHORIZED)
  }
  let user = await User.findOne({id: token.user.id})
  if (!user) {
    return new NotFoundError('user_not_found')
  }
  let req = {}
  req['ipAddress_auto_generated'] = token.ipAddress
  req['userAgent_auto_generated'] = token.userAgent

  return createTokens(client, token.user.id, req)
}

// Public method for logout
let revokeToken = async function (tokenId, all) {
  let token = await Token.findOne({token: tokenId}, {relations: ['client', 'user']})
  if (!token) {
    return new NotFoundError('Token not found')
  }
  if (all) {
    await Token.delete({user: {id: token.user.id}, client: {id: token.client.id}})
  } else {
    await Token.delete(token.id)
  }
  return true
}

// Token endpoint
export const token = [
  server.token(),
  server.errorHandler()
]
