import * as Binance from 'node-binance-api'
import APIError from '../helpers/APIError'
import * as httpStatus from 'http-status'
import { pickBy } from 'lodash'
import * as marketMinimums from './../../minimums.json'

export default class MarketController {
  static async getMarkets(req, res) {
    const {user} = req
    if (!user.validBinanceKey) {
      throw new APIError(`Your binance credentials are invalid. Please update them.`, httpStatus.BAD_REQUEST)
    }
    const binanceApi = new Binance().options({
      APIKEY: req.user.binanceApiKey,
      APISECRET: req.user.binanceSecretKey
    })
    const prices = await binanceApi.prices()
    let markets = pickBy(prices, (price, market) => market.toString().endsWith('USDT'))
    return res.json({markets, minimums: marketMinimums})
  }

  static async getMarketHoldings(req, res) {
    const {user} = req
    if (!user.validBinanceKey) {
      throw new APIError(`Your binance credentials are invalid. Please update them.`, httpStatus.BAD_REQUEST)
    }
    const binanceApi = new Binance().options({
      APIKEY: req.user.binanceApiKey,
      APISECRET: req.user.binanceSecretKey
    })
    const prices = await binanceApi.prices()
    const balances = await binanceApi.balance()
    const balanceMarkets = Object.keys(balances)
    const nonEmptyBalances = Object.values(balances).map((balance: any, index: number) => ({
      ...balance,
      market: balanceMarkets[index],
      price: balanceMarkets[index] === 'USDT' ? 1 : prices[`${balanceMarkets[index]}USDT`]
    })).filter((balance: any) => balance.available > 0 && balance.onOrder > 0)
    return res.json({balances: nonEmptyBalances})
  }
}
