import { Router } from 'express'
import UserController from '../controllers/user.controller'
import { catchErrors } from '../helpers/errorHelpers'
import { isAuthenticated, isOptionalAuthenticated } from '../middlewares/authentication'

/**
 * @swagger
 * tags:
 *     name: User
 *     description: All the routes affecting User
 */
/**
 * @swagger
 * definitions:
 *   User:
 *     type: object
 *     properties:
 *       user:
 *         type: object
 *         properties:
 *           email:
 *             type: string
 *           password:
 *             type: string
 */
/**
 * @swagger
 * components:
 *   requestBodies:
 *     UserBody:
 *      description: A JSON object containing user information
 *      required: true
 *      content:
 *        application/json:
 *          schema:
 *            $ref: '#/definitions/User'
 */
const router = Router()
/**
 * @swagger
 * /users/me:
 *    get:
 *      summary: This shall return the authenticated user.
 *      responses:
 *        200:
 *           description: fetch authenticated user
 *      tags:
 *        - User
 */
router.get('/me', isAuthenticated, catchErrors(UserController.getCurrentUser))

/**
 * @swagger
 * /users/:
 *    post:
 *      summary: This shall create and entry in Users table with the sent data.
 *      requestBody:
 *        $ref: '#/components/requestBodies/UserBody'
 *      responses:
 *        200:
 *           description: create user
 *      tags:
 *        - User
 */
router.post('/', isOptionalAuthenticated, catchErrors(UserController.saveUser))
/**
 * @swagger
 * /users/notification-token/{device}/{token}:
 *    post:
 *      summary: This shall register a notification token for the logged in user based on device
 *      parameters:
 *        - in: path
 *          name: device
 *          schema:
 *            type: string
 *          required: true
 *          description: device os
 *        - in: path
 *          name: token
 *          schema:
 *            type: string
 *          required: true
 *          description: notification token
 *      responses:
 *        200:
 *           description: subscribe to push notifications
 *      tags:
 *        - User
 */
router.post('/notification-token/:device/:token', isAuthenticated, catchErrors(UserController.registerNotificationToken))
/**
 * @swagger
 * /users/me:
 *    put:
 *      summary: This shall update the logged in User.
 *      parameters:
 *        - in: path
 *          name: id
 *          schema:
 *            type: string
 *          required: true
 *          description: User id
 *      requestBody:
 *        $ref: '#/components/requestBodies/UserBody'
 *      responses:
 *        200:
 *           description: update user
 *      tags:
 *        - User
 */
router.put('/me',
  isAuthenticated,
  catchErrors(UserController.updateUser)
)

export default router
