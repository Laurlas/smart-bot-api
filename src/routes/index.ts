import { Router } from 'express'

import authRoutes from './auth.route'
import userRoutes from './user.route'
import botRoutes from './bot.route'
import marketRoutes from './market.route'

export const router = Router()

router.use('/', authRoutes)
router.use('/users', userRoutes)
router.use('/bots', botRoutes)
router.use('/markets', marketRoutes)

export default router
