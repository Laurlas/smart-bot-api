import { Router } from 'express'
import { isAuthenticated, isClientAuthenticated } from '../middlewares/authentication'
import * as oauth2Controller from '../controllers/oauth2.controller'

const api = Router()

/**
 * @swagger
 * tags:
 *     name: Auth
 *     description: All the routes for authentication
 */

/**
 * @swagger
 * definitions:
 *   Auth:
 *     type: object
 *     properties:
 *       client_id:
 *         type: string
 *       client_secret:
 *         type: string
 *       grant_type:
 *         type: string
 *         enum: [password, client_credentials, refresh_token, authorization_code]
 *       username:
 *         type: string
 *       password:
 *         type: string
 *       refresh_token:
 *         type: string
 *       code:
 *         type: string
 *   ForgotPassword:
 *     type: object
 *     properties:
 *       client_id:
 *         type: string
 *       client_secret:
 *         type: string
 *       grant_type:
 *         type: string
 *         enum: [client_credentials]
 *       email:
 *         type: string
 *   ResetPassword:
 *     type: object
 *     properties:
 *       client_id:
 *         type: string
 *       client_secret:
 *         type: string
 *       grant_type:
 *         type: string
 *         enum: [client_credentials]
 *       token:
 *         type: string
 *       newPassword:
 *         type: string
 */

/**
 * @swagger
 * components:
 *   requestBodies:
 *     AuthBody:
 *      description: A JSON object containing authentication details
 *      required: true
 *      content:
 *        application/json:
 *          schema:
 *            $ref: '#/definitions/Auth'
 *     ForgotPasswordBody:
 *      description: A JSON object containing forgot password request details
 *      required: true
 *      content:
 *        application/json:
 *          schema:
 *            $ref: '#/definitions/ForgotPassword'
 *     ResetPasswordBody:
 *      description: A JSON object containing reset password request details
 *      required: true
 *      content:
 *        application/json:
 *          schema:
 *            $ref: '#/definitions/ResetPassword'
 */

/**
 * @swagger
 * /oauth2/token:
 *    post:
 *      summary: This shall create a authentication token.
 *      requestBody:
 *        $ref: '#/components/requestBodies/AuthBody'
 *      responses:
 *        200:
 *           description: create token
 *      tags:
 *        - Auth
 */
api.post('/oauth2/token', isClientAuthenticated, oauth2Controller.token)

/**
 * @swagger
 * /oauth2/logout:
 *    post:
 *      summary: This shall delete the token associated to the user.
 *      responses:
 *        200:
 *           description: delete user tokens
 *      tags:
 *        - Auth
 */
api.post('/oauth2/logout', isAuthenticated, oauth2Controller.doLogout)

export default api
