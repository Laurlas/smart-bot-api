import { Router } from 'express'
import BotController from '../controllers/bot.controller'
import { catchErrors } from '../helpers/errorHelpers'
import { isAuthenticated } from '../middlewares/authentication'

/**
 * @swagger
 * tags:
 *     name: Bot
 *     description: All the routes affecting Bot
 */
/**
 * @swagger
 * definitions:
 *   Bot:
 *     type: object
 *     properties:
 *       bot:
 *         type: object
 *         properties:
 *           market:
 *             type: string
 *           ordersDistance:
 *             type: number
 *           sellProfit:
 *             type: number
 *           investment:
 *             type: number
 *   BotDelete:
 *     type: object
 *     properties:
 *       sell:
 *         type: boolean
 */
/**
 * @swagger
 * components:
 *   requestBodies:
 *     BotBody:
 *      description: A JSON object containing bot information
 *      required: true
 *      content:
 *        application/json:
 *          schema:
 *            $ref: '#/definitions/Bot'
 *     BotDeleteBody:
 *      description: A JSON object containing bot delete information
 *      required: true
 *      content:
 *        application/json:
 *          schema:
 *            $ref: '#/definitions/BotDelete'
 */
const router = Router()

/**
 * @swagger
 * /bots/{id}:
 *    get:
 *      summary: This shall return the bot with the respective {id}.
 *      parameters:
 *        - in: path
 *          name: id
 *          schema:
 *            type: string
 *          required: true
 *          description: Bot id
 *      responses:
 *        200:
 *           description: fetch bot
 *      tags:
 *        - Bot
 */
router.get('/:id', isAuthenticated, catchErrors(BotController.getBot))

/**
 * @swagger
 * /bots:
 *    get:
 *      summary: This shall return the bots listing.
 *      responses:
 *        200:
 *           description: fetch bots
 *      tags:
 *        - Bot
 */
router.get('/', isAuthenticated, catchErrors(BotController.getBots))

/**
 * @swagger
 * /bots/:
 *    post:
 *      summary: This shall create and entry in Bots table with the sent data.
 *      requestBody:
 *        $ref: '#/components/requestBodies/BotBody'
 *      responses:
 *        200:
 *           description: create bot
 *      tags:
 *        - Bot
 */
router.post('/', isAuthenticated, catchErrors(BotController.saveBot))
/**
 * @swagger
 * /bots/{id}:
 *    delete:
 *      summary: This shall remove a bot.
 *      requestBody:
 *        $ref: '#/components/requestBodies/BotDeleteBody'
 *      responses:
 *        200:
 *           description: remove bot
 *      tags:
 *        - Bot
 */
router.delete('/:id', isAuthenticated, catchErrors(BotController.closeBot))

export default router
