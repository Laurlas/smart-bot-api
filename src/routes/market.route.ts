import { Router } from 'express'
import MarketController from '../controllers/market.controller'
import { catchErrors } from '../helpers/errorHelpers'
import { isAuthenticated } from '../middlewares/authentication'

/**
 * @swagger
 * tags:
 *     name: Market
 *     description: All the routes affecting Market
 */

const router = Router()

/**
 * @swagger
 * /markets/holdings:
 *    get:
 *      summary: This shall return the markets holdings.
 *      responses:
 *        200:
 *           description: fetch markets holdings
 *      tags:
 *        - Market
 */
router.get('/holdings', isAuthenticated, catchErrors(MarketController.getMarketHoldings))
/**
 * @swagger
 * /markets:
 *    get:
 *      summary: This shall return the markets listing.
 *      responses:
 *        200:
 *           description: fetch markets
 *      tags:
 *        - Market
 */
router.get('/', isAuthenticated, catchErrors(MarketController.getMarkets))

export default router
