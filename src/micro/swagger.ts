import * as swaggerJSDoc from 'swagger-jsdoc'

let swaggerDefinition = {
  openapi: '3.0.1',
  info: {
    title: 'Smart Bot API',
    version: '0.0.2',
    description: 'Smart Bot API specs'
  },
  host: '',
  servers: [
    {
      url: '/api'
    }
  ],
  components: {
    securitySchemes: {
      bearerAuth: {
        type: 'http',
        scheme: 'bearer',
        bearerFormat: 'JWT'
      }
    }
  },
  security: [{
    bearerAuth: []
  }]

}

let options = {
  swaggerDefinition: swaggerDefinition,
  apis: ['./**/routes/*.ts']// pass all in array
}

export const swaggerSpec = swaggerJSDoc(options)
