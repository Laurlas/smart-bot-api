import {
  registerDecorator,
  ValidationOptions,
  ValidationArguments,
} from 'class-validator'

export function MinOrUndefined(property: number, validationOptions?: ValidationOptions) {
  return (object: any, propertyName: string) => {
    registerDecorator({
      name: 'MinOrUndefined',
      target: object.constructor,
      propertyName,
      constraints: [],
      options: validationOptions,
      validator: {
        validate(value: any, args: ValidationArguments) {
          return value === undefined || value >= property
        },
        defaultMessage(args: ValidationArguments) {
          return `${propertyName} cannot be lower than ${property}`
        }
      }
    })
  }
}

export function LengthOrUndefined(min: number, max: number, validationOptions?: ValidationOptions) {
  return (object: any, propertyName: string) => {
    registerDecorator({
      name: 'MinOrUndefined',
      target: object.constructor,
      propertyName,
      constraints: [],
      options: validationOptions,
      validator: {
        validate(value: any, args: ValidationArguments) {
          return value === undefined || ((typeof value === 'string' || value instanceof String) && value.length >= min && value.length <= max)
        },
        defaultMessage(args: ValidationArguments) {
          return `${propertyName} length bust be between ${min} and ${max}`
        }
      }
    })
  }
}

export function IsEmailOrUndefined(validationOptions?: ValidationOptions) {
  return (object: any, propertyName: string) => {
    registerDecorator({
      name: 'IsEmailOrUndefined',
      target: object.constructor,
      propertyName,
      constraints: [],
      options: validationOptions,
      validator: {
        validate(value: any, args: ValidationArguments) {
          return value === undefined || validateEmail(value)
        },
        defaultMessage(args: ValidationArguments) {
          return `${propertyName} must be a valid email`
        }
      }
    })
  }
}

function validateEmail(email) {
  let re = /^(([^<>()\[\]\.,;:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i
  return re.test(String(email).toLowerCase())
}

export function DateAfter(property: string, validationOptions?: ValidationOptions) {
  return function (object: Object, propertyName: string) {
    registerDecorator({
      name: 'dateAfter',
      target: object.constructor,
      propertyName: propertyName,
      constraints: [property],
      options: validationOptions,
      validator: {
        validate(value: any, args: ValidationArguments) {
          const [relatedPropertyName] = args.constraints
          const relatedValue = (args.object as any)[relatedPropertyName]
          if (typeof relatedValue !== 'string' || typeof value !== 'string') return false
          const start = new Date(value.toString())
          const end = new Date(relatedValue.toString())

          return start.getTime() > end.getTime()
        },
        defaultMessage(args: ValidationArguments) {
          return '$property must higher than ' + args.constraints[0]
        }
      }
    })
  }
}

export function DateBefore(property: string, validationOptions?: ValidationOptions) {
  return function (object: Object, propertyName: string) {
    registerDecorator({
      name: 'dateBefore',
      target: object.constructor,
      propertyName: propertyName,
      constraints: [property],
      options: validationOptions,
      validator: {
        validate(value: any, args: ValidationArguments) {
          const [relatedPropertyName] = args.constraints
          const relatedValue = (args.object as any)[relatedPropertyName]
          if (typeof relatedValue !== 'string' || typeof value !== 'string') return false
          const start = new Date(value.toString())
          const end = new Date(relatedValue.toString())
          return start.getTime() < end.getTime()
        },
        defaultMessage(args: ValidationArguments) {
          return '$property must be lower than ' + args.constraints[0]
        }
      }
    })
  }
}

export function DateAfterNow(validationOptions?: ValidationOptions) {
  return (object: any, propertyName: string) => {
    registerDecorator({
      name: 'DateAfterNow',
      target: object.constructor,
      propertyName,
      constraints: [],
      options: validationOptions,
      validator: {
        validate(value: any, args: ValidationArguments) {
          const target = new Date(value.toString())
          const now = new Date()
          return target.getTime() > now.getTime()
        },
        defaultMessage(args: ValidationArguments) {
          return `${propertyName} must be a date after now`
        }
      }
    })
  }
}
