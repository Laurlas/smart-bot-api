import { Bot } from '../entities/Bot'
import { createConnection } from 'typeorm'
import { getMarketDetails } from '../helpers/binanceHelper'
import * as Binance from 'node-binance-api'
import { Order, OrderType } from '../entities/Order'
import { countDecimals, toFixedFloor } from '../helpers/parsers'
import expoService from '../helpers/ExpoService'

const getBotsRemainingInvestment = async () => {
  return Bot.query(`
    select b.id,
           b.market,
           b.ordersDistance,
           b.sellProfit,
           investment + SUM(
            IFNULL(IF(o.type = ?, - o.value - o.commission * o.exchangeRate, o.value - o.commission * o.exchangeRate), 0)
           ) as remainingInvestment,
           binanceApiKey,
           binanceSecretKey,
           b.userId
    from bots b
             inner join users u on b.userId = u.id and u.validBinanceKey = true
             left join orders o on b.id = o.botId and (o.type = ? or o.filledAt <> null)
    where b.deletedAt is null
    group by b.id
  `, [OrderType.BUY, OrderType.BUY])
}
const tryPlaceSellOrder = async (binanceApi, unpairedBuyOrder, botDetails, availableCoins, marketPrice, marketDetails) => {
  try {
    let toSellPrice = toFixedFloor((100 + botDetails.sellProfit) * unpairedBuyOrder.exchangeRate / 100, countDecimals(marketDetails.tickSize))
    const sellQuantity = toFixedFloor(unpairedBuyOrder.amount - unpairedBuyOrder.commission, countDecimals(marketDetails.stepSize))
    if (parseFloat(sellQuantity) < availableCoins) {
      let sellResult
      if (marketPrice > toSellPrice) {
        sellResult = await binanceApi.marketSell(botDetails.market, sellQuantity)
        console.info(`Placing sell order ${sellQuantity} ${botDetails.market} at ${marketPrice} USDT`)
        await expoService.sendPushNotificationToInternalUser(botDetails.userId, 'New sell order placed!', `Placed to sell ${sellQuantity} ${botDetails.market} at ${marketPrice} USDT`)
      } else {
        sellResult = await binanceApi.sell(botDetails.market, sellQuantity, toSellPrice)
        console.info(`Placing sell order ${sellQuantity} ${botDetails.market} at ${toSellPrice} USDT`)
        await expoService.sendPushNotificationToInternalUser(botDetails.userId, 'New sell order placed!', `Placed to sell ${sellQuantity} ${botDetails.market} at ${toSellPrice} USDT`)
      }
      const {executedQty, fills, orderId} = sellResult
      const commission = fills.reduce((a, b) => a + parseFloat(b?.commission || 0), 0)
      const avgPrice = (fills.reduce((a, b) => a + parseFloat(b?.price || marketPrice), 0)) / fills.length
      const value = (fills.reduce((a, b) => a + parseFloat(b?.price || marketPrice) * parseFloat(b.qty), 0)) / fills.length
      let internalSellOrder
      if (sellResult.status === 'FILLED') {
        internalSellOrder = Order.create({
          type: OrderType.SELL,
          exchangeRate: avgPrice,
          amount: parseFloat(executedQty),
          value,
          commission,
          filledAt: new Date(),
          externalId: orderId,
          bot: {id: botDetails.id}
        })
      } else {
        internalSellOrder = Order.create({
          type: OrderType.SELL,
          exchangeRate: toSellPrice,
          amount: sellResult.origQty,
          value: toSellPrice * sellResult.origQty,
          commission: parseFloat(sellResult.origQty) / 1000,
          filledAt: null,
          externalId: orderId,
          bot: {id: botDetails.id}
        })
      }
      internalSellOrder = await Order.save(internalSellOrder)
      await Order.update({id: unpairedBuyOrder.id}, {sellOrder: {id: internalSellOrder.id}})
      availableCoins -= internalSellOrder.amount
      return availableCoins
    }
  } catch (e) {
    console.error(e?.body || e)
  }
}
const tryPlaceSellOrders = async (botDetails) => {
  const binanceApi = new Binance().options({
    APIKEY: botDetails.binanceApiKey,
    APISECRET: botDetails.binanceSecretKey
  })
  const marketPrice = parseFloat((await binanceApi.prices(botDetails.market))[botDetails.market])
  const marketDetails = await getMarketDetails(binanceApi, botDetails.market)

  const balances = await binanceApi.balance()
  const coin = botDetails.market.replace('USDT', '')
  if (balances[coin] && balances[coin].available) {
    let availableCoins = parseFloat(balances[coin].available)
    const unpairedBuyOrders = await Order.query(`
    select id,
      exchangeRate,
      amount,
      commission
    from orders
    where type = ?
      and botId = ?
      and sellOrderId is null
      and amount - commission < ?
  `, [OrderType.BUY, botDetails.id, availableCoins])
    for (let i = 0; i < unpairedBuyOrders.length; i++) {
      availableCoins = await tryPlaceSellOrder(binanceApi, unpairedBuyOrders[i], botDetails, availableCoins, marketPrice, marketDetails)
    }
  }
}
const tryPlaceBuyOrders = async (botDetails) => {
  try {
    const binanceApi = new Binance().options({
      APIKEY: botDetails.binanceApiKey,
      APISECRET: botDetails.binanceSecretKey
    })
    const marketDetails = await getMarketDetails(binanceApi, botDetails.market)
    const buyValue = parseFloat(marketDetails.minNotional) * 1.1
    if (botDetails.remainingInvestment < buyValue) {
      return
    }

    const balances = await binanceApi.balance()
    const usdtBalance = parseFloat(balances['USDT'] && balances['USDT'].available)
    if (usdtBalance < buyValue) {
      return
    }
    const [{min, max}] = await Order.query(`
    select min(exchangeRate) as min, max(exchangeRate) as max
    from orders
    where botId = ?
      and type = ?
      and filledAt is null
  `, [botDetails.id, OrderType.BUY])

    const marketPrice = parseFloat((await binanceApi.prices(botDetails.market))[botDetails.market])
    let toBuyPrice
    const firstLowerBuyPrice = toFixedFloor((100 - botDetails.ordersDistance) * marketPrice / 100, countDecimals(marketDetails.tickSize))
    if (max && min) {
      const lowerLimitPrice = toFixedFloor((100 + botDetails.ordersDistance) * max / 100, countDecimals(marketDetails.tickSize))
      if (lowerLimitPrice < firstLowerBuyPrice && firstLowerBuyPrice < marketPrice) {
        toBuyPrice = firstLowerBuyPrice
      } else {
        toBuyPrice = toFixedFloor((100 - botDetails.ordersDistance) * min / 100, countDecimals(marketDetails.tickSize))
      }
    } else {
      toBuyPrice = firstLowerBuyPrice
    }
    const toBuyQuantity = toFixedFloor(buyValue / toBuyPrice, countDecimals(marketDetails.stepSize))
    console.info(`Buying ${toBuyQuantity} ${botDetails.market} worth ${toBuyQuantity * toBuyPrice} USDT`)
    const limitBuyResult = await binanceApi.buy(botDetails.market, toBuyQuantity, toBuyPrice)
    let internalBuyLimitOrder = Order.create({
      type: OrderType.BUY,
      exchangeRate: parseFloat(limitBuyResult.price),
      amount: toBuyQuantity,
      value: toBuyQuantity * parseFloat(limitBuyResult.price),
      commission: parseFloat(limitBuyResult.origQty) / 1000,
      filledAt: null,
      externalId: limitBuyResult.orderId,
      bot: {id: botDetails.id}
    })
    internalBuyLimitOrder = await Order.save(internalBuyLimitOrder)
    await expoService.sendPushNotificationToInternalUser(botDetails.userId, 'New buy order placed!', `Placed ${toBuyQuantity} ${botDetails.market} worth ${toBuyQuantity * toBuyPrice} USDT`)
  } catch (e) {
    console.error(e?.body || e)
  }
}
const tryPlaceOrders = async (botDetails) => {
  await Promise.all([
    tryPlaceSellOrders(botDetails),
    tryPlaceBuyOrders(botDetails)
  ])
}
const start = async () => {
  const remainingBotsRemainingInvestments = await getBotsRemainingInvestment()
  for (let i = 0; i < remainingBotsRemainingInvestments.length; i++) {
    await tryPlaceOrders(remainingBotsRemainingInvestments[i])
  }
}
(async () => {
  try {
    await createConnection()
    await start()
  } catch (e) {
    console.error(e)
  }
  process.exit(0)
})()