import { createConnection } from 'typeorm'
import { Bot } from '../entities/Bot'
import * as Binance from 'node-binance-api'
import { Order } from '../entities/Order'
const syncOrder = async (binanceApi, order) => {
  try {
    const orderStatus = await binanceApi.orderStatus(order.market, order.externalId)
    if (orderStatus.status === 'FILLED') {
      await Order.update({id: order.id}, {filledAt: new Date()})
    }
  } catch (e) {
    console.error(e?.body || e)
  }
}
const syncOrders = async ({binanceApiKey, binanceSecretKey, orders}) => {
  const binanceApi = new Binance().options({
    APIKEY: binanceApiKey,
    APISECRET: binanceSecretKey
  })
  for (let i = 0; i < orders.length; i++) {
    await syncOrder(binanceApi, orders[i])
  }
}
const getUnfilledOrders = async () => {
  return Bot.query(`
    select externalId, o.id, binanceApiKey, binanceSecretKey, market, b.userId
    from orders o
             inner join bots b on o.botId = b.id and b.deletedAt is null
             inner join users u on b.userId = u.id and validBinanceKey = ?
    where filledAt is null
    order by binanceApiKey
  `, [true])
}
const start = async () => {
  const unfilledOrders = await getUnfilledOrders()
  let groupedOrders = {}
  for (let i = 0; i < unfilledOrders.length; i++) {
    if (!groupedOrders[unfilledOrders[i].binanceApiKey]) {
      groupedOrders[unfilledOrders[i].binanceApiKey] = {
        binanceApiKey: unfilledOrders[i].binanceApiKey,
        binanceSecretKey: unfilledOrders[i].binanceSecretKey,
        userId: unfilledOrders[i].userId,
        orders: []
      }
    }
    groupedOrders[unfilledOrders[i].binanceApiKey].orders.push({
      id: unfilledOrders[i].id,
      externalId: unfilledOrders[i].externalId,
      userId: unfilledOrders[i].userId,
      market: unfilledOrders[i].market
    })
  }
  const groupedOrdersList = Object.values(groupedOrders) as any
  for (let i = 0; i < groupedOrdersList.length; i++) {
    await syncOrders(groupedOrdersList[i])
  }
}
(async () => {
  try {
    await createConnection()
    await start()
  } catch (e) {
    console.error(e)
  }
  process.exit(0)
})()