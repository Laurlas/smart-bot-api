module.exports = {
  type: process.env.TYPEORM_CONNECTION,
  host: process.env.TYPEORM_HOST,
  port: process.env.TYPEORM_PORT,
  username: process.env.TYPEORM_USERNAME,
  password: process.env.TYPEORM_PASSWORD,
  database: process.env.TYPEORM_DATABASE,
  synchronize: process.env.TYPEORM_SYNCHRONIZE,
  migrationsRun: process.env.TYPEORM_MIGRATIONS_RUN,
  logging: process.env.TYPEORM_LOGGING,
  entities: [process.env.TYPEORM_ENTITIES],
  migrations: [process.env.TYPEORM_MIGRATIONS],
  factories: [process.env.TYPEORM_FACTORIES],
  seeds: [process.env.TYPEORM_SEEDS],
  subscribers: [process.env.TYPEORM_SUBSCRIBERS],
  cli: {
    migrationsDir: process.env.TYPEORM_MIGRATIONS_DIR
  }
}
